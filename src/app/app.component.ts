import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-root',
  template: `
  <div *ngIf='tester == true'>
  <nav class="navbar navbar-light bg-faded">
    <a class="navbar-brand" href="#">Admin panel</a>
    <ul class="nav navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="supportedContentDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
          <div class="dropdown-menu" aria-labelledby="supportedContentDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
      </ul>
      <form class="form-inline float-xs-right">
        <input class="form-control" type="text" placeholder="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </nav>
    <br>
    <div class='container'>
      <input type="text" class="form-control" #email placeholder="Введите emeil" />
      <br>
      <input type="text" class="form-control" #password placeholder="Введите пароль " />
      <br>
      <button class='btn btn-outline-secondary btn-lg btn-block' (click)="register(email.value,password.value)">Зарегестрироваться</button>
      <hr>
      <input type="text" class="form-control" #emais placeholder="Введите emeil" />
      <br>
      <input type="text" class="form-control" #passwords placeholder="Введите пароль " />
      <br>
      <button class='btn btn-outline-secondary btn-lg btn-block' (click)="login(emais.value,passwords.value)">Войти</button>
      <hr>
      <button class="btn btn-outline-warning" (click)="runs(tester, moder)">Если зашел под модером</button>
      <button class="btn btn-outline-warning" (click)="runss(tester, admin)">Если зашел под админом</button>
    </div>
    <!--<div class='container' >
      <h1>EMAIL:<div class="alert alert-success" role="alert">
        <strong>{{ (user | async)?.email }}</strong>
      </div></h1>
        <h1>STATUS:<div class="alert alert-success" role="alert">
        <strong>{{ (user | async)?.status }}</strong>
      </div></h1>
        {{ (user | async)?.email }}
        {{ (user | async)?.status }}
      <hr>
      <input type="text" class="form-control" placeholder="Что сделать?" #newname>
      <br>
      <input type="text" class="form-control" placeholder="Опишите задание..." #newtext>
      <br>
      <input type="text" class="form-control" placeholder="Как с вами связаться?" #newcont>
      <br>
      <button class="btn btn-outline-info btn-lg btn-block" (click)="save(newname.value,newtext.value, newcont.value)">Заказать</button>
      <br>
      <button class='btn btn-outline-danger' (click)='logOut()'>Покеда</button>
      <a href='https://www.google.ru/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=%D0%9A%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B1%D1%83%D0%B1%D0%B5%D0%BD%20%D0%B4%D0%BB%D1%8F%20%D0%B8%D0%B7%D0%B3%D0%BD%D0%B0%D0%BD%D0%B8%D1%8F%20%D1%81%D0%BE%D1%82%D0%B0%D0%BD%D1%8B%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D1%82%D1%8E%D0%BC%D0%B5%D0%BD%D1%8C'>На всякий случай</a>
    </div>-->
  </div>


  <div  *ngIf='tester == false'>
  <div class='container'>
    <br>
    <input type="text" class="form-control" placeholder="Что сделать?" #newname>
    <br>
    <input type="text" class="form-control" placeholder="Опишите задание..." #newtext>
    <br>
    <input type="text" class="form-control" placeholder="Как с вами связаться?" #newcont>
    <br>
    <button class="btn btn-outline-info btn-lg btn-block" (click)="save(newname.value,newtext.value, newcont.value)">Заказать</button>
    <br>
  </div>
  <hr>
  <div class='container'>
    <button class="btn btn-outline-warning" (click)="run(tester)">Я не юзерь</button>
    <button class='btn btn-outline-danger' (click)='logOut()'>Обнулить</button>
  </div>
  <!-- -->
  </div>


  <div *ngIf='moder == true'>
  <nav class="navbar navbar-light bg-faded">
    <a class="navbar-brand" href="#">Menager panel</a>
    <ul class="nav navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="supportedContentDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
          <div class="dropdown-menu" aria-labelledby="supportedContentDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
      </ul>
      <form class="form-inline float-xs-right">
        <input class="form-control" type="text" placeholder="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </nav>
    <br>
    <br>
    <div>
      <h4>Показать:</h4>
      <button class="btn btn-outline-primary" (click)="filterBy('new')">Новые</button>
      <button class="btn btn-outline-info" (click)="filterBy('perform')">Выполняемые</button>
      <button class="btn btn-outline-success" (click)="filterBy('complited')">Законченные</button>
    </div>
    <ul>
      <li *ngFor="let item of items | async">
        <p>Задание:</p>
        <input type="text" class="form-control" #updatea [value]="item.task" />
        <br>
        <p>Описание:</p>
        <input type="text" class="form-control" #updateb [value]="item.about" />
        <br>
        <p>Связь:</p>
        <input type="text" class="form-control" #updatec [value]="item.contact" />
        <br>
        <p>Статус:</p>
        <input type="text" id="disabledInput" class="form-control" #updated [value]="item.status" />
        <br>
        <button class="btn btn-success" (click)="updateItem(item.$key, updatea.value)">Одобрить</button>
        <button class="btn btn-info" (click)="updateItems(item.$key, updatea.value , updateb.value, updatec.value)">Отредактировать</button>
        <button class="btn btn-danger" (click)="deletef(item.$key, updatea.value, updateb.value, updatec.value, updated.value)">Удалить</button>
        <hr>
      </li>
    </ul>
  </div>


  <div *ngIf='admin == true'>
  <nav class="navbar navbar-light bg-faded">
    <a class="navbar-brand" href="#">Admin panel</a>
    <ul class="nav navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="supportedContentDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
          <div class="dropdown-menu" aria-labelledby="supportedContentDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
      </ul>
      <form class="form-inline float-xs-right">
        <input class="form-control" type="text" placeholder="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </nav>
    <br>
    <br>
    <div class='container'>
    <ul style='text-decoration:none'>
      <li *ngFor="let item of items | async" style='list-style-type: none;'>
        <a href="#" class="list-group-item list-group-item-action">
        <h5 class="list-group-item-heading" #updatei>{{item.task}}</h5>
        <br>
        <p class="list-group-item-text" #updatef>{{item.about}}</p>
        <hr>
        <span #updateg>{{item.contact}}</span>
        <br>
        <button class="btn btn-success" (click)="updateIte(item.$key, updatei.value)">Сделано! Мне нужны денюжки!</button>
        <button class="btn btn-warning" (click)="deleteff(item.$key, updatei.value)">Отказаться. (Удаляется безвозратно.)</button>
         </a>
        <hr>
      </li>
    </ul>
    </div>
    <div class='text-xs-center'>
    <button class="btn btn-info" (click)="filterBy('perform')">Выполняемые</button>
    <button class="btn btn-success" (click)="filterBy('complited')">Проводится оплата...</button>
    <button class="btn btn-warning" (click)="filterBy('deleted')">Удаленные</button>
    </div>
    <hr>
    <button class="btn btn-outline-danger">Список пользователей</button>
  </div>
  `,
})
export class AppComponent {
  items: FirebaseListObservable<any[]>;
  ones: FirebaseListObservable<any[]>;
  sizeSubject: Subject<any>;
  oneSubject: Subject<any>;
  emailSubject: Subject<any>;
  user: FirebaseObjectObservable<any>;
  stat: FirebaseObjectObservable<any>;
  goee: FirebaseListObservable<any>;
  history: FirebaseListObservable<any>;
  constructor(public af: AngularFire) {
    this.sizeSubject = new Subject();
    this.items = af.database.list('/task', {
     query: {
       orderByChild: 'status',
       equalTo: this.sizeSubject
     }
   });
    this.goee = af.database.list('/task');
    this.history = af.database.list('/history');
    this.af.auth.subscribe(auth => {
      if(auth){
        console.log('auth success');
        this.user = this.af.database.object('users/' + auth.uid);
        this.stat = this.af.database.object('users/' + auth.uid + '/status' , { preserveSnapshot: true });
        this.stat.subscribe(snapshot => {
            console.log(snapshot.val());
            this.logOut();
            let statusss = snapshot.val();
            if(statusss == 'user'){
              console.log('You are user');
              this.tester = false;
              alert('Error.');
            }else if(statusss == 'moder'){
              console.log('You are moderator');
              this.moder = true;
              this.admin = false;
                  //Не слушайте TypeScript, тут все норм!
              this.tester = 'crack';
                  //Не слушайте TypeScript, тут все норм!
            }else if(statusss == 'admin'){
              console.log('You are administrator');
              this.admin = true;
              this.moder = false;
                  //Не слушайте TypeScript, тут все норм!
              this.tester = 'crack';
                  //Не слушайте TypeScript, тут все норм!
            }else{
              console.log('Error');
            }
        });

      }
    })
  }
  moder = false;
  admin = false;
  save(newName: string,newNames: string,newNamess: string) {
    this.goee.push({ task: newName, about: newNames, contact: newNamess, status: 'new' });
  }
  updateItems(key: string, newText: string, newh: string, newcon: string ) {
    this.items.update(key, { task: newText, about: newh, contact: newcon });
  }
  run(tester) {
    this.tester = true;
  }
  runs(tester, moder) {
    //Не слушайте TypeScript, тут все норм!
    this.tester = 'crack';
    //Не слушайте TypeScript, тут все норм!
    this.moder = true;
  }
  runss(tester, admin) {
    //Не слушайте TypeScript, тут все норм!
    this.tester = 'crack';
    //Не слушайте TypeScript, тут все норм!
    this.admin = true;
  }
  deletef(key: string, task: string, about: string, contact: string){
    console.log(task);
    console.log(about);
    console.log(contact);
    task = 'deleted'
    console.log(task);
    this.items.update(key, { status: task });
  }
  deleteff(key: string, task: string){
    console.log('Удаление!');
    this.items.remove(key);
  }
  updateItem(key: string, newText: string) {
   console.log(newText);
   newText = 'perform';
   console.log(newText);
   this.items.update(key, { status: newText });
   console.log(key);

 }
 updateIte(key: string, newText: string){
   console.log(newText);
   newText = 'complited';
   console.log(newText);
   this.items.update(key, { status: newText });
   console.log(key);
 }
  filterBy(size: string) {
    this.sizeSubject.next(size);
    console.log('Поиск в базе - ' + size);
  }
  filterByss(size: string) {
    this.oneSubject.next(size);
    console.log('Поиск в базе - ' + size);
  }
  logOut() {
    this.af.auth.logout();
    this.tester = false;
    console.log('Pokeda')
  }
  overrideLogin() {
    this.af.auth.login({
      provider: AuthProviders.Anonymous,
      method: AuthMethods.Anonymous,
    });
  }
  login(email: string, password: string){
    this.af.auth.login({ email: email, password: password })

  }
  register(email: string, password: string){
    this.af.auth.createUser({
      email: email,
      password: password
    }).then(data=>{
      console.log('data',data);
      if(data.auth.email === email ){
        this.af.database.object('users/'+data.auth.uid).set({email:data.auth.email, status: 'user'})
        console.log('В базу отправлено: ' + email);
        console.log('В базу отправлено:' + data.auth.uid);
        console.log('Аккаунт успешно зарегистрирован!');
      }
    });
  }
  tester = false;
}
