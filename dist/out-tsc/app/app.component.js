var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Subject } from 'rxjs/Subject';
export var AppComponent = (function () {
    function AppComponent(af) {
        var _this = this;
        this.af = af;
        this.moder = false;
        this.admin = false;
        this.tester = false;
        this.sizeSubject = new Subject();
        this.items = af.database.list('/task', {
            query: {
                orderByChild: 'status',
                equalTo: this.sizeSubject
            }
        });
        this.goee = af.database.list('/task');
        this.history = af.database.list('/history');
        this.af.auth.subscribe(function (auth) {
            if (auth) {
                console.log('auth success');
                _this.user = _this.af.database.object('users/' + auth.uid);
                _this.stat = _this.af.database.object('users/' + auth.uid + '/status', { preserveSnapshot: true });
                _this.stat.subscribe(function (snapshot) {
                    console.log(snapshot.val());
                    _this.logOut();
                    var statusss = snapshot.val();
                    if (statusss == 'user') {
                        console.log('You are user');
                        _this.tester = false;
                        alert('Error.');
                    }
                    else if (statusss == 'moder') {
                        console.log('You are moderator');
                        _this.moder = true;
                        _this.admin = false;
                        _this.tester = 'crack';
                    }
                    else if (statusss == 'admin') {
                        console.log('You are administrator');
                        _this.admin = true;
                        _this.moder = false;
                        _this.tester = 'crack';
                    }
                    else {
                        console.log('Error');
                    }
                });
            }
        });
    }
    AppComponent.prototype.save = function (newName, newNames, newNamess) {
        this.goee.push({ task: newName, about: newNames, contact: newNamess, status: 'new' });
    };
    AppComponent.prototype.updateItems = function (key, newText, newh, newcon) {
        this.items.update(key, { task: newText, about: newh, contact: newcon });
    };
    AppComponent.prototype.run = function (tester) {
        this.tester = true;
    };
    AppComponent.prototype.runs = function (tester, moder) {
        this.tester = 'crack';
        this.moder = true;
    };
    AppComponent.prototype.runss = function (tester, admin) {
        this.tester = 'crack';
        this.admin = true;
    };
    AppComponent.prototype.deletef = function (key, task, about, contact) {
        console.log(task);
        console.log(about);
        console.log(contact);
        task = 'deleted';
        console.log(task);
        this.items.update(key, { status: task });
    };
    AppComponent.prototype.deleteff = function (key, task) {
        console.log('Удаление!');
        this.items.remove(key);
    };
    AppComponent.prototype.updateItem = function (key, newText) {
        console.log(newText);
        newText = 'perform';
        console.log(newText);
        this.items.update(key, { status: newText });
        console.log(key);
    };
    AppComponent.prototype.updateIte = function (key, newText) {
        console.log(newText);
        newText = 'complited';
        console.log(newText);
        this.items.update(key, { status: newText });
        console.log(key);
    };
    AppComponent.prototype.filterBy = function (size) {
        this.sizeSubject.next(size);
        console.log('Поиск в базе - ' + size);
    };
    AppComponent.prototype.filterByss = function (size) {
        this.oneSubject.next(size);
        console.log('Поиск в базе - ' + size);
    };
    AppComponent.prototype.logOut = function () {
        this.af.auth.logout();
        this.tester = false;
        console.log('Pokeda');
    };
    AppComponent.prototype.overrideLogin = function () {
        this.af.auth.login({
            provider: AuthProviders.Anonymous,
            method: AuthMethods.Anonymous,
        });
    };
    AppComponent.prototype.login = function (email, password) {
        this.af.auth.login({ email: email, password: password });
    };
    AppComponent.prototype.register = function (email, password) {
        var _this = this;
        this.af.auth.createUser({
            email: email,
            password: password
        }).then(function (data) {
            console.log('data', data);
            if (data.auth.email === email) {
                _this.af.database.object('users/' + data.auth.uid).set({ email: data.auth.email, status: 'user' });
                console.log('В базу отправлено: ' + email);
                console.log('В базу отправлено:' + data.auth.uid);
                console.log('Аккаунт успешно зарегистрирован!');
            }
        });
    };
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            template: "\n  <div *ngIf='tester == true'>\n  <nav class=\"navbar navbar-light bg-faded\">\n    <a class=\"navbar-brand\" href=\"#\">Admin panel</a>\n    <ul class=\"nav navbar-nav\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item dropdown\">\n        <a class=\"nav-link dropdown-toggle\" href=\"http://example.com\" id=\"supportedContentDropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Dropdown</a>\n          <div class=\"dropdown-menu\" aria-labelledby=\"supportedContentDropdown\">\n            <a class=\"dropdown-item\" href=\"#\">Action</a>\n            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n          </div>\n        </li>\n      </ul>\n      <form class=\"form-inline float-xs-right\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Search\">\n        <button class=\"btn btn-outline-success\" type=\"submit\">Search</button>\n      </form>\n    </nav>\n    <br>\n    <div class='container'>\n      <input type=\"text\" class=\"form-control\" #email placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 emeil\" />\n      <br>\n      <input type=\"text\" class=\"form-control\" #password placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C \" />\n      <br>\n      <button class='btn btn-outline-secondary btn-lg btn-block' (click)=\"register(email.value,password.value)\">\u0417\u0430\u0440\u0435\u0433\u0435\u0441\u0442\u0440\u0438\u0440\u043E\u0432\u0430\u0442\u044C\u0441\u044F</button>\n      <hr>\n      <input type=\"text\" class=\"form-control\" #emais placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 emeil\" />\n      <br>\n      <input type=\"text\" class=\"form-control\" #passwords placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C \" />\n      <br>\n      <button class='btn btn-outline-secondary btn-lg btn-block' (click)=\"login(emais.value,passwords.value)\">\u0412\u043E\u0439\u0442\u0438</button>\n      <hr>\n      <button class=\"btn btn-outline-warning\" (click)=\"runs(tester, moder)\">\u0415\u0441\u043B\u0438 \u0437\u0430\u0448\u0435\u043B \u043F\u043E\u0434 \u043C\u043E\u0434\u0435\u0440\u043E\u043C</button>\n      <button class=\"btn btn-outline-warning\" (click)=\"runss(tester, admin)\">\u0415\u0441\u043B\u0438 \u0437\u0430\u0448\u0435\u043B \u043F\u043E\u0434 \u0430\u0434\u043C\u0438\u043D\u043E\u043C</button>\n    </div>\n    <!--<div class='container' >\n      <h1>EMAIL:<div class=\"alert alert-success\" role=\"alert\">\n        <strong>{{ (user | async)?.email }}</strong>\n      </div></h1>\n        <h1>STATUS:<div class=\"alert alert-success\" role=\"alert\">\n        <strong>{{ (user | async)?.status }}</strong>\n      </div></h1>\n        {{ (user | async)?.email }}\n        {{ (user | async)?.status }}\n      <hr>\n      <input type=\"text\" class=\"form-control\" placeholder=\"\u0427\u0442\u043E \u0441\u0434\u0435\u043B\u0430\u0442\u044C?\" #newname>\n      <br>\n      <input type=\"text\" class=\"form-control\" placeholder=\"\u041E\u043F\u0438\u0448\u0438\u0442\u0435 \u0437\u0430\u0434\u0430\u043D\u0438\u0435...\" #newtext>\n      <br>\n      <input type=\"text\" class=\"form-control\" placeholder=\"\u041A\u0430\u043A \u0441 \u0432\u0430\u043C\u0438 \u0441\u0432\u044F\u0437\u0430\u0442\u044C\u0441\u044F?\" #newcont>\n      <br>\n      <button class=\"btn btn-outline-info btn-lg btn-block\" (click)=\"save(newname.value,newtext.value, newcont.value)\">\u0417\u0430\u043A\u0430\u0437\u0430\u0442\u044C</button>\n      <br>\n      <button class='btn btn-outline-danger' (click)='logOut()'>\u041F\u043E\u043A\u0435\u0434\u0430</button>\n      <a href='https://www.google.ru/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=%D0%9A%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B1%D1%83%D0%B1%D0%B5%D0%BD%20%D0%B4%D0%BB%D1%8F%20%D0%B8%D0%B7%D0%B3%D0%BD%D0%B0%D0%BD%D0%B8%D1%8F%20%D1%81%D0%BE%D1%82%D0%B0%D0%BD%D1%8B%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D1%82%D1%8E%D0%BC%D0%B5%D0%BD%D1%8C'>\u041D\u0430 \u0432\u0441\u044F\u043A\u0438\u0439 \u0441\u043B\u0443\u0447\u0430\u0439</a>\n    </div>-->\n  </div>\n\n\n  <div  *ngIf='tester == false'>\n  <div class='container'>\n    <br>\n    <input type=\"text\" class=\"form-control\" placeholder=\"\u0427\u0442\u043E \u0441\u0434\u0435\u043B\u0430\u0442\u044C?\" #newname>\n    <br>\n    <input type=\"text\" class=\"form-control\" placeholder=\"\u041E\u043F\u0438\u0448\u0438\u0442\u0435 \u0437\u0430\u0434\u0430\u043D\u0438\u0435...\" #newtext>\n    <br>\n    <input type=\"text\" class=\"form-control\" placeholder=\"\u041A\u0430\u043A \u0441 \u0432\u0430\u043C\u0438 \u0441\u0432\u044F\u0437\u0430\u0442\u044C\u0441\u044F?\" #newcont>\n    <br>\n    <button class=\"btn btn-outline-info btn-lg btn-block\" (click)=\"save(newname.value,newtext.value, newcont.value)\">\u0417\u0430\u043A\u0430\u0437\u0430\u0442\u044C</button>\n    <br>\n  </div>\n  <hr>\n  <div class='container'>\n    <button class=\"btn btn-outline-warning\" (click)=\"run(tester)\">\u042F \u043D\u0435 \u044E\u0437\u0435\u0440\u044C</button>\n    <button class='btn btn-outline-danger' (click)='logOut()'>\u041E\u0431\u043D\u0443\u043B\u0438\u0442\u044C</button>\n  </div>\n  <!-- -->\n  </div>\n\n\n  <div *ngIf='moder == true'>\n  <nav class=\"navbar navbar-light bg-faded\">\n    <a class=\"navbar-brand\" href=\"#\">Menager panel</a>\n    <ul class=\"nav navbar-nav\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item dropdown\">\n        <a class=\"nav-link dropdown-toggle\" href=\"http://example.com\" id=\"supportedContentDropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Dropdown</a>\n          <div class=\"dropdown-menu\" aria-labelledby=\"supportedContentDropdown\">\n            <a class=\"dropdown-item\" href=\"#\">Action</a>\n            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n          </div>\n        </li>\n      </ul>\n      <form class=\"form-inline float-xs-right\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Search\">\n        <button class=\"btn btn-outline-success\" type=\"submit\">Search</button>\n      </form>\n    </nav>\n    <br>\n    <br>\n    <div>\n      <h4>\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C:</h4>\n      <button class=\"btn btn-outline-primary\" (click)=\"filterBy('new')\">\u041D\u043E\u0432\u044B\u0435</button>\n      <button class=\"btn btn-outline-info\" (click)=\"filterBy('perform')\">\u0412\u044B\u043F\u043E\u043B\u043D\u044F\u0435\u043C\u044B\u0435</button>\n      <button class=\"btn btn-outline-success\" (click)=\"filterBy('complited')\">\u0417\u0430\u043A\u043E\u043D\u0447\u0435\u043D\u043D\u044B\u0435</button>\n    </div>\n    <ul>\n      <li *ngFor=\"let item of items | async\">\n        <p>\u0417\u0430\u0434\u0430\u043D\u0438\u0435:</p>\n        <input type=\"text\" class=\"form-control\" #updatea [value]=\"item.task\" />\n        <br>\n        <p>\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435:</p>\n        <input type=\"text\" class=\"form-control\" #updateb [value]=\"item.about\" />\n        <br>\n        <p>\u0421\u0432\u044F\u0437\u044C:</p>\n        <input type=\"text\" class=\"form-control\" #updatec [value]=\"item.contact\" />\n        <br>\n        <p>\u0421\u0442\u0430\u0442\u0443\u0441:</p>\n        <input type=\"text\" id=\"disabledInput\" class=\"form-control\" #updated [value]=\"item.status\" />\n        <br>\n        <button class=\"btn btn-success\" (click)=\"updateItem(item.$key, updatea.value)\">\u041E\u0434\u043E\u0431\u0440\u0438\u0442\u044C</button>\n        <button class=\"btn btn-info\" (click)=\"updateItems(item.$key, updatea.value , updateb.value, updatec.value)\">\u041E\u0442\u0440\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C</button>\n        <button class=\"btn btn-danger\" (click)=\"deletef(item.$key, updatea.value, updateb.value, updatec.value, updated.value)\">\u0423\u0434\u0430\u043B\u0438\u0442\u044C</button>\n        <hr>\n      </li>\n    </ul>\n  </div>\n\n\n  <div *ngIf='admin == true'>\n  <nav class=\"navbar navbar-light bg-faded\">\n    <a class=\"navbar-brand\" href=\"#\">Admin panel</a>\n    <ul class=\"nav navbar-nav\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Link</a>\n      </li>\n      <li class=\"nav-item dropdown\">\n        <a class=\"nav-link dropdown-toggle\" href=\"http://example.com\" id=\"supportedContentDropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Dropdown</a>\n          <div class=\"dropdown-menu\" aria-labelledby=\"supportedContentDropdown\">\n            <a class=\"dropdown-item\" href=\"#\">Action</a>\n            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n          </div>\n        </li>\n      </ul>\n      <form class=\"form-inline float-xs-right\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Search\">\n        <button class=\"btn btn-outline-success\" type=\"submit\">Search</button>\n      </form>\n    </nav>\n    <br>\n    <br>\n    <div class='container'>\n    <ul style='text-decoration:none'>\n      <li *ngFor=\"let item of items | async\" style='list-style-type: none;'>\n        <a href=\"#\" class=\"list-group-item list-group-item-action\">\n        <h5 class=\"list-group-item-heading\" #updatei>{{item.task}}</h5>\n        <br>\n        <p class=\"list-group-item-text\" #updatef>{{item.about}}</p>\n        <hr>\n        <span #updateg>{{item.contact}}</span>\n        <br>\n        <button class=\"btn btn-success\" (click)=\"updateIte(item.$key, updatei.value)\">\u0421\u0434\u0435\u043B\u0430\u043D\u043E! \u041C\u043D\u0435 \u043D\u0443\u0436\u043D\u044B \u0434\u0435\u043D\u044E\u0436\u043A\u0438!</button>\n        <button class=\"btn btn-warning\" (click)=\"deleteff(item.$key, updatei.value)\">\u041E\u0442\u043A\u0430\u0437\u0430\u0442\u044C\u0441\u044F. (\u0423\u0434\u0430\u043B\u044F\u0435\u0442\u0441\u044F \u0431\u0435\u0437\u0432\u043E\u0437\u0440\u0430\u0442\u043D\u043E.)</button>\n         </a>\n        <hr>\n      </li>\n    </ul>\n    </div>\n    <div class='text-xs-center'>\n    <button class=\"btn btn-info\" (click)=\"filterBy('perform')\">\u0412\u044B\u043F\u043E\u043B\u043D\u044F\u0435\u043C\u044B\u0435</button>\n    <button class=\"btn btn-success\" (click)=\"filterBy('complited')\">\u041F\u0440\u043E\u0432\u043E\u0434\u0438\u0442\u0441\u044F \u043E\u043F\u043B\u0430\u0442\u0430...</button>\n    <button class=\"btn btn-warning\" (click)=\"filterBy('deleted')\">\u0423\u0434\u0430\u043B\u0435\u043D\u043D\u044B\u0435</button>\n    </div>\n    <hr>\n    <button class=\"btn btn-outline-danger\">\u0421\u043F\u0438\u0441\u043E\u043A \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u0435\u0439</button>\n  </div>\n  ",
        }), 
        __metadata('design:paramtypes', [AngularFire])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=../../../src/app/app.component.js.map